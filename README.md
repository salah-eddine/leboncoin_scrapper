# leboncoin_scrapper

#Front (front folder)

### `npm install`

Install front-end app dependencies

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#Back (back folder)

### `npm install`

Install back-end app dependencies including puppeteer

### `npm start`

Runs the back end server on [http://localhost:4000](http://localhost:4000)

Here we have 3 routes:

- to run the scrapper go to route [http://localhost:4000/launch-scrapper](http://localhost:4000/launch-scrapper)
- to find all scrapped cars go to route [http://localhost:4000/cars](http://localhost:4000/cars)
- to find a single car by id go to route http://localhost:4000/cars/:id
