import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Header from './Header'
import Home from './Home'
import Cars from './Cars'
import Car from './Car'

const App = () => {
  return (
    <Router>
      <>
        <Header />

        <Route exact path="/" component={Home} />
        <Route exact path="/cars" component={Cars} />
        <Route path="/cars/:id" component={Car} />
      </>
    </Router>
  )
}

export default App
