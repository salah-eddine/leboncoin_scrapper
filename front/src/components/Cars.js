import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import api from '../api/api'

const Cars = ({ match }) => {
  const [cars, setCars] = useState([])
  useEffect(() => {
    api.get('/cars').then(response => setCars(response.data))
  }, [])
  return (
    <div className="ui divided items">
      {cars.map(car => (
        <div className="item" key={car._id}>
          <div className="image">
            <img src={car.images[0]} alt={car.title} />
          </div>
          <div className="content">
            <Link to={`${match.url}/${car._id}`} className="header">
              {car.title}
            </Link>
            <div className="meta">
              <span className="cinema">{car.price}</span>
            </div>
            <div className="description">{car.description}</div>
            <div className="extra">
              <Link to={`${match.url}/${car._id}`}>
                <div className="ui right floated primary button">
                  Details
                  <i className="right chevron icon" />
                </div>
              </Link>
            </div>
          </div>
        </div>
      ))}
    </div>
  )
}

export default Cars
