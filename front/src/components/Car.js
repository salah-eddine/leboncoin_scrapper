import React, { useState, useEffect } from 'react'
import api from '../api/api'

const Car = ({ match }) => {
  const [car, setCar] = useState({})
  useEffect(() => {
    api.get(`/cars/${match.params.id}`).then(response => setCar(response.data))
  }, [match.params.id])
  return (
    car && (
      <div className="ui segment">
        <h3>{car.title}</h3>
        <h3>Price: {car.price}</h3>
        <h3>Description: </h3>
        <p>{car.description}</p>
        <h3> {car.date}</h3>
        <h3>Vendeur: {car.seller}</h3>
        <h3>Localisation: {car.localisation}</h3>
        <h3>Phone: {car.phone}</h3>
        {car.criteria && (
          <>
            <h3>Criteria</h3>
            <ul>
              {Object.keys(car.criteria).map(key => (
                <li key={key}>
                  {key}: {car.criteria[key]}
                </li>
              ))}
            </ul>
          </>
        )}
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          {car.images &&
            car.images.map((image, index) => (
              <img
                key={`img-${index}`}
                style={{ width: '500px', margin: '5px' }}
                src={image}
                alt={car.title}
              />
            ))}
        </div>
      </div>
    )
  )
}

export default Car
