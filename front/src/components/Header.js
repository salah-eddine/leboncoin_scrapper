import React from 'react'
import { Link } from 'react-router-dom'

const Header = () => {
  return (
    <div class="ui three item menu">
      <Link to="/" className="item">
        Home
      </Link>
      <Link to="/cars" className="item">
        Cars
      </Link>
    </div>
  )
}

export default Header
