const puppeteer = require('puppeteer')

module.exports = async () => {
  const getAllUrl = async browser => {
    const page = await browser.newPage()
    await page.goto('https://www.leboncoin.fr/recherche/?category=2&text=tesla')
    await page.waitFor('body')
    return await page.evaluate(() =>
      Array.from(
        document.querySelectorAll(
          '#container > main > div > div > div.l17WS.bgMain > div > div._2Njaz._3GLp9 > div._358dQ > div > div:nth-child(1) > div:nth-child(2) > ul > li > a'
        )
      ).map(a => a.href)
    )
  }

  const getDataFromUrl = async (browser, url) => {
    const page = await browser.newPage()

    await page.goto(url, { timeout: 60000 })
    await page.waitFor('body')

    let result = await page.evaluate(() => {
      const getInnerText = selector => {
        if (selector) return selector.innerText
        return null
      }
      const title = getInnerText(document.querySelector('h1'))
      const price = getInnerText(document.querySelector('.eVLNz span'))
      const criteria = Array.from(
        document.querySelectorAll(
          '#container > main > div > div > div > section > section > section.OjX8R > div:nth-child(5) > div:nth-child(2) > div > div'
        )
      ).reduce((map, element) => {
        const innerTextArray = element.innerText.split('\n')
        map[innerTextArray[0]] = innerTextArray[1]
        return map
      }, {})
      const description = getInnerText(document.querySelector('.content-CxPmi'))
      const date = getInnerText(
        document.querySelector(
          '#container > main > div > div > div > section > section._35sFG > section.OjX8R > div._2NKYa > div._3aOPO > div._14taM > div:nth-child(3)'
        )
      )
      let seller = getInnerText(
        document.querySelector(
          '#container > main > div > div > div > section > section > section._1_H-h > div._2-Dyg._2-42k > div > div > div > div._1_Utw > div._2zgX4 > div._2rGU1'
        )
      )
      if (seller === null) {
        seller = getInnerText(
          document.querySelector(
            '#container > main > div > div > div > section > section > section._1_H-h > div._2-Dyg._2-42k > div > div > div > div:nth-child(1) > div._1PTTu > div > div > a > div'
          )
        )
      }
      const localisation = getInnerText(
        document.querySelector(
          '#container > main > div > div > div > section > section > section.OjX8R > div:nth-child(7) > div:nth-child(2) > div > div > div._1aCZv > span'
        )
      )
      const images = Array.from(
        document.querySelectorAll(
          '#container > main > div > div > div > section > section._35sFG > section.OjX8R > div._2NKYa > div:nth-child(1) > div > div.GwNx3 > div > div._2mHu7 > div > div > span > div'
        )
      )
        .map(element => {
          const backgroundImage = element.style.backgroundImage
          return backgroundImage.substring(5, backgroundImage.length - 2)
        })
        .filter(image => image !== 'ne')

      const phoneButtonSelector = document.querySelector(
        '#container > main > div > div > div > section > section > section._1_H-h > div._2-Dyg._2-42k > div > div > div > div.YTu2J > div > div:nth-child(1) > div > div > button'
      )
      if (phoneButtonSelector) {
        phoneButtonSelector.click()
      }

      return {
        title,
        price,
        criteria,
        description,
        date,
        seller,
        localisation,
        images,
      }
    })

    await page.waitFor(500)

    const phone = await page.evaluate(() => {
      const selector = document.querySelector(
        '#container > main > div > div > div > section > section > section._1_H-h > div._2-Dyg._2-42k > div > div > div > div.YTu2J > div > div:nth-child(1) > div > div > div > span > a'
      )
      return selector ? selector.innerText : null
    })

    result = { ...result, phone }

    return result
  }

  const browser = await puppeteer.launch({ headless: false })
  const urlList = await getAllUrl(browser)
  const results = await Promise.all(
    urlList.map(url => getDataFromUrl(browser, url))
  )
  browser.close()
  return results
}
