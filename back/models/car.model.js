const mongoose = require('mongoose')

const CarSchema = mongoose.Schema({
  title: String,
  price: String,
  criteria: {
    MARQUE: String,
    MODÈLE: String,
    'ANNÉE-MODÈLE': String,
    KILOMÉTRAGE: String,
    CARBURANT: String,
    'BOÎTE DE VITESSE': String,
  },
  description: String,
  date: String,
  seller: String,
  localisation: String,
  images: [String],
  phone: String,
})

module.exports = mongoose.model('Car', CarSchema, 'CarStore')
