const express = require('express')
const cors = require('cors')
const _ = require('lodash')
const mongoose = require('mongoose')
const Car = require('./models/car.model')
const scrapper = require('./scrapper')

const app = express()
app.use(cors())
const port = 4000

const DB_USER = 'scrapper_user'
const DB_PASSWORD = '[fBJ8AWHX]R-Z(bs'
const DB_URI = `mongodb://${DB_USER}:${DB_PASSWORD}@ds153906.mlab.com:53906/scrapper`
mongoose.connect(DB_URI, { useNewUrlParser: true })
var db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function() {
  console.log('Database successfully connected')
})

app.get('/launch-scrapper', async (req, res) => {
  const scrappedCars = await scrapper()
  Car.insertMany(scrappedCars, (err, cars) => {
    if (err) res.json(400).send(err)
    else res.status(201).send(scrappedCars)
  })
})

app.get('/cars', (req, res) => {
  Car.find((err, cars) => {
    if (err) return res.json(err)
    res.status(200).send(cars)
  })
})

app.get('/cars/:id', (req, res) => {
  const id = req.params.id
  Car.findById(id, (err, car) => {
    if (err) return res.json(err)
    res.status(200).send(car)
  })
})

app.listen(port, () =>
  console.log(`leboncoin_scrapper listening on port ${port}!`)
)
